#pragma once
#include <string>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
using namespace std;

class Word
{
public:
	Word();
	~Word();

	string wordArr[10] = { 
		"norway", 
		"dark", 
		"greenland", 
		"englang", 
		"michelangelo", 
		"nile",
		"everest",
		"sahara",
		"japan",
		"damaskus"
	};

	string question[10] = {
		"Country Known as the Land of Midnight Sun", 
		"Low Light", 
		"Biggest Island in the World", 
		"First Industrial Revolution took Place in", 
		"Last Judgement was Painted by",
		"Longest River in the World",
		"Highest Mountain in the World",
		"Biggest Dessert in the World",
		"Country of the Rising Sun",
		"World's Oldest Known City"
	};

	int randWord();
};

