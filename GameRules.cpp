#include "GameRules.h"



GameRules::GameRules()
{
}


GameRules::~GameRules()
{
}

void GameRules::GamePlay()
{
	int x = 1;
	
	while (true) {
		cout << "Term " << x << endl;
		cout << "Your Score : " << this->score << endl;
		this->n = w.randWord();
		Term(w.wordArr[n]);

		if (this->changeQuest) {
			this->n = w.randWord();
			cout << "Changing the Question. \n";
			Term(w.wordArr[n]);
			x--;
			this->changeQuest = false;
		}

		if (this->lifes <= 0) {
			//cout << "You've done for." << endl;
			break;
		}
		x++;
	}

}

void GameRules::Term(string word)
{
	this->lifes = 3;
	int exposed = 0;
	string display = word;
	for (int i = 0; i < display.length(); i++) {
		display[i] = '*';
	}

	bool help1 = false;
	bool help2 = false;
	bool help3 = false;

	while (exposed < word.length())
	{
		cout << "Your Life(s) : " << lifes << endl;
		cout << "Clue : " << w.question[n] << endl;
		cout << "Enter a letter in word ";
		cout << display << " : ";
		char response;
		cin >> response;

		bool guessTrue = false;
		bool duplicate = false;
		bool helping = true;
		for (int i = 0; i < word.length(); i++) {
			if (response == word[i]) {
				if (display[i] == word[i]) {
					cout << response << " is already opened. \n";
					duplicate = true;
					break;
				}
				else {
					display[i] = word[i];
					exposed++;
					guessTrue = true;
				}

			}
			else if (response == '?') {
				
				int help;
				int randhint = randomHint(word);
				cout << "1. Hint \n" << "2. Reset Life \n" << "3. Change Question \n";
				cout << "Enter your Help action (1/2/3) : ";
				cin >> help;
				cout << endl;

				switch (help)
				{
				case 1:
					if (help1) {
						cout << "This option has already taken \n";
						guessTrue = true;
						helping = false;
						break;
					}
					else {
						display[randhint] = word[randhint];
						exposed++;
						guessTrue = true;
						helping = false;
						help1 = true;
						break;
					}	
				case 2:
					if (help2) {
						cout << "This option has already taken \n";
						guessTrue = true;
						helping = false;
						break;
					}
					else {
						this->lifes = 3;
						guessTrue = true;
						helping = false;
						help2 = true;
						break;
					}
				case 3:
					if (help3) {
						cout << "This option has already taken \n";
						guessTrue = true;
						helping = false;
						break;
					}
					else {
						this->changeQuest = true;
						guessTrue = true;
						helping = false;
						help3 = true;
						break;
					}	
				default:
					break;
				}
			} 

			if (!helping)
				break;
		}

		if (this->changeQuest)
			break;

		if (duplicate)
			continue;

		if (!guessTrue) {
			this->lifes--;
			cout << response << " is wrong. \n";
		}

		if (this->lifes < 0) {
			cout << "You've done for." << endl;
			cout << "Your Score : " << score << endl; 
			break;
		}

	}

	if (!this->changeQuest) {

		if (lifes == 3)
			this->score += 100;
		else if (lifes == 2)
			this->score += 90;
		else if (lifes == 1)
			this->lifes += 80;
		else if (lifes == 0)
			this->score += 70;

		cout << "The Word was " << '"' << word << '"' << endl;

		system("pause");

		cout << endl << endl;
	}

	

}

int GameRules::randomHint(string rWord)
{
	//string display = rWord;
	int random = rand() % rWord.length();
	return random;
	
}
