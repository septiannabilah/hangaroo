#pragma once
#include <string>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
using namespace std;

#include "Word.h";

class GameRules
{

public:
	GameRules();
	~GameRules();

	int lifes;

	void GamePlay();
	void Term(string word);
	int randomHint(string w);

private:
	Word w;
	int n;
	int score = 0;
	bool changeQuest = false;

};

